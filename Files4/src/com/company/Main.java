package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        String[] students = new String[5];

        students[0] = "Hugo";
        students[1] = "Paco";
        students[2] = "Juanjo";
        students[3] = "Milhouse";
        students[4] = "Antonio";

        insertStudents(students);

        deleteStudents();
    }

    private static void insertStudents(String[] students) {

        String directory = "resources";
        String fileName = "fitxer4.txt";

        File file = new File(directory, fileName);

        try {

            FileWriter fileWriter = new FileWriter(file);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (int i = 0; i < students.length; i++) {

                bufferedWriter.write(students[i] + "\n");
                bufferedWriter.flush();
            }

        } catch(IOException e) {

            e.printStackTrace();
        }
    }

    private static void deleteStudents() {

        String directory = "resources";
        String fileName = "fitxer4.txt";

        File file = new File(directory, fileName);

        try {

            String line;

            String lineToRemove = "Antonio";

            FileWriter fileWriter = new FileWriter(file);
            FileReader fileReader = new FileReader(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null){

                String trimmedLine = line.trim();

                if(trimmedLine.equals(lineToRemove)) continue;

                bufferedWriter.write(line + System.getProperty("line.separator"));

            }

            bufferedWriter.close();
            bufferedReader.close();

        } catch(IOException e) {

            e.printStackTrace();
        }

    }
}
